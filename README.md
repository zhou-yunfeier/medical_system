# medical_system

#### 介绍
本项目为基于SpringBoot的医疗信息管理系统，原为武汉理工计算机学院JavaEE课程大作业，因时间有限，功能有待完善，
后续会继续完善问诊、支付宝微信缴费等相关功能，并在gitee中更新https://gitee.com/zhou-yunfeier/medical_system
项目部署在腾讯云服务器上，通过http://43.142.84.89:8080/sel/login 可进行访问

#### 软件架构
SpringBoot 框架
MySQL 数据库
LayUI 前端框架
Maven 项目管理工具
shiro Java安全框架


#### 安装教程

1.  下载安装 Navicat（MySQL操作工具，非常方便好用） http://www.navicat.com.cn/download/navicat-premium 
2.  下载安装 IDEA开发环境 https://www.jetbrains.com/idea/download/#section=windows；
3.  下载安装 JDK 1.8
4.  下载并安装 MySQL，下载地址：https://dev.mysql.com/downloads/installer/
    设置数据库帐号：root 密码：123456；



#### 本地运行使用说明

1.  在cmd中启动mysql服务；
2.  在Navicat中新建本地mysql连接，并且新建数据库test；
3.  下载./sql文件夹中的hospital.sql文件，右击medical数据库，运行该文件，即可导入数据库；
4.  下载本项目解压并导入IDEA中；
5.  在IDEA右侧导航栏Maven中选中hospital/Lifecycle，双击clean，双击install；
6.  IDEA上方Edit Configurations，如没有自动生成，须手动设置主程序入口类HospitalApplication；
7.  点击run，等待console出现“HospitalApplication成功启动! 请在浏览器键入 localhost:8080 后回车访问”字样；
8.  打开浏览器，输入“localhost:8080”，enter跳转进入医疗信息管理系统首页；
9.  账号：admin 密码：123456

