package com.aaa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalApplication {

    private final static Logger logger = LoggerFactory.getLogger(HospitalApplication.class);
    public static void main(String[] args) {

        SpringApplication.run(HospitalApplication.class, args);
        logger.info("HospitalApplication 成功启动!");
        logger.info("请在浏览器键入 localhost:8080 后回车访问");

    }

}
